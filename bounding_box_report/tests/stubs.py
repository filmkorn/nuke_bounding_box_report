import random


class InfoStub(object):
    def __init__(self):

        self._w = random.randint(100, 160000)
        self._h = random.randint(100, 160000)

    def w(self):
        return self._w

    def h(self):
        return self._h


class NodeStub(object):
    def __init__(self):
        self._bbox = InfoStub()

    def bbox(self):
        return self._bbox
