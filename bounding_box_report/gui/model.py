from Qt import QtCore
from Qt import QtGui

from bounding_box_report import nuke_utils


class BoundingBoxTableModel(QtCore.QAbstractTableModel):

    _HEADERS = ("width", "height", "total pixels", "channels")

    def __init__(self, nodes=None):
        """Init the model.

        Args:
            nodes (List[nuke.Node]): Nodes to represent.

        """
        super(BoundingBoxTableModel, self).__init__()

        self._nodes = nodes or []
        self._palette = QtGui.QPalette()

    @property
    def nodes(self):
        """List[nuke.Node]: Nodes of the model."""
        return self._nodes

    def rowCount(self, parent=QtCore.QModelIndex()):
        """Number of rows in this model.

        Args:
            parent (QtCore.QModelIndex, optional): Parent index.

        Returns:
            int: Number of nodes.

        """
        if parent.isValid():
            return 0

        return len(self.nodes)

    def columnCount(self, parent=QtCore.QModelIndex()):
        """Static number of columns represented by this model."""
        if parent.isValid():
            return 0

        return len(self._HEADERS)

    def get_background_color(self, row, node):
        """Return the cell color.

        If a knob is animated, return colors matching Nuke's property panel.
        Else blend the nodes color with the apps palette color at certain
        amounts, depending on weather the node has a knob or not.

        Args:
            row (int): Row to get background color for.
            node (nuke.Node): Node to get color from.

        Returns:
            QtGui.QBrush: Color of the current cell.

        """

        def scalar(tpl, multiplier):
            """Multiply each value in tuple by scalar.

            Examples:
                >>> scalar((1.0, 2.0, 3.0), 2.0)
                (2.0, 4.0, 6.0)

            Args:
                tpl (tuple|list): Values to multiply.
                multiplier (float): Multiplier.

            Returns:
                tuple: tpl * multiplier

            """
            return tuple([multiplier * t for t in tpl])

        color = nuke_utils.get_node_tile_color(node)
        if not row % 2:
            base = self._palette.base().color()  # type: QtGui.QColor
        else:
            base = self._palette.alternateBase().color()

        mix = 0.2

        base_color = base.getRgbF()[:3]

        # Blend Nodes color with base color
        base_color_blend = scalar(base_color, 1.0 - mix)
        color_blend = scalar(color, mix)
        color = [sum(x) for x in zip(base_color_blend, color_blend)]
        return QtGui.QBrush(QtGui.QColor().fromRgbF(*color))

    def headerData(self, section, orientation, role):
        """Returns the header data.

        Args:
            section (QtCore.int): return headerData for this section
            orientation (QtCore.Qt.Orientation): header orientation
            role (QtCore.int): the current role.

                * QtCore.Qt.DisplayRole: name of node or knob


        Returns:
            str: The header names.

        """
        if orientation == QtCore.Qt.Vertical:
            node = self._nodes[section]
            if not nuke_utils.node_exists(node):
                return super(BoundingBoxTableModel, self).headerData(
                    section, orientation, role
                )

            if role == QtCore.Qt.BackgroundRole:
                return QtGui.QBrush(
                    QtGui.QColor.fromRgbF(*(nuke_utils.get_node_tile_color(node)))
                )
            elif role == QtCore.Qt.ForegroundRole:
                return QtGui.QPen(
                    QtGui.QColor.fromRgbF(*(nuke_utils.get_node_font_color(node)))
                )

        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self._HEADERS[section]
            elif orientation == QtCore.Qt.Vertical:
                return self._nodes[section].fullName()

        if role == QtCore.Qt.UserRole:
            if orientation == QtCore.Qt.Vertical:
                return self._nodes[section]

    def data(self, index, role):
        """Returns the header data.

        For UserRole this returns the node or knob, depending on given
        orientation.

        Args:
            index (QtCore.QModelIndex): return headerData for this index
            role (QtCore.int): The current role.

        """
        row, column = index.row(), index.column()

        if not self.nodes:
            return

        node = self.nodes[row]

        # Return early if node was deleted to prevent access to detached
        # python node object.
        if not nuke_utils.node_exists(node):
            # self.removeRows(parent=QtCore.QModelIndex(),
            #                 row=row,
            #                 count=1)
            return

        bbox = node.bbox()
        channels = len(node.channels())

        if role == QtCore.Qt.DisplayRole:
            if column == 0:
                return int(bbox.w())
            elif column == 1:
                return int(bbox.h())
            elif column == 2:
                return int(bbox.w() * bbox.h())
            elif column == 3:
                return channels

        elif role == QtCore.Qt.BackgroundRole:
            return self.get_background_color(row, node)

    # def sort(self, column, order=QtCore.Qt.AscendingOrder):
    #     self.
    #     if column == 0:
    #         self._nodes = sorted(self.)
