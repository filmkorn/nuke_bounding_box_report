"""Show and update the GUI."""

import nuke
from Qt import QtCore

from bounding_box_report.gui import model
from bounding_box_report.gui import view
from bounding_box_report import nuke_utils


class Controller(object):
    def __init__(self, nodes=None):
        self._nodes = nodes

    def get_gui(self, parent=None):
        self._model = model.BoundingBoxTableModel()
        self._sort_model = QtCore.QSortFilterProxyModel()
        self._sort_model.setSourceModel(self._model)

        self._view = view.BoundingBoxReportWidget(parent)
        self._view.table_view.setModel(self._sort_model)

        self.connect_signals()

        return self._view

    def show_gui(self, parent=None):
        self.get_gui()
        self._view.show()

    def connect_signals(self):
        self._view.load_all_action.triggered.connect(self.load_all)
        self._view.load_selected_action.triggered.connect(self.load_selected)

    def _update_model(self):
        self._model = model.BoundingBoxTableModel(self._nodes)
        self._sort_model.setSourceModel(self._model)

        self._model.layoutChanged.emit()

    def load_all(self):
        self._nodes = nuke.allNodes(recurseGroups=True)
        self._update_model()

    def load_selected(self):
        self._nodes = nuke_utils.get_selected_nodes(True)
        self._update_model()
