"""Widgets of the bouding box report."""
from Qt import __binding__
from Qt import QtCore
from Qt import QtGui
from Qt import QtWidgets
import nuke

from bounding_box_report import nuke_utils


class BoundingBoxReportTableView(QtWidgets.QTableView):
    def __init__(self, parent=None):
        super(BoundingBoxReportTableView, self).__init__(parent)

        self.setAlternatingRowColors(True)
        self.setSortingEnabled(True)

        self.setHorizontalScrollMode(QtWidgets.QTableView.ScrollPerPixel)
        self.setVerticalScrollMode(QtWidgets.QTableView.ScrollPerPixel)

        self.header_view = NodeHeaderView()
        self.setVerticalHeader(self.header_view)


class BoundingBoxReportWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(BoundingBoxReportWidget, self).__init__(parent)

        self.setMinimumWidth(500)

        self._layout = QtWidgets.QVBoxLayout(self)
        self._layout.setContentsMargins(0, 0, 0, 0)

        self.menu_bar = QtWidgets.QMenuBar(self)
        # Show menubar in parents window for OSX and some linux dists.
        self.menu_bar.setNativeMenuBar(False)
        self._layout.addWidget(self.menu_bar)

        self.load_all_action = QtWidgets.QAction(
            "All Nodes",
            self.menu_bar,
        )
        self.menu_bar.addAction(self.load_all_action)

        self.load_selected_action = QtWidgets.QAction(
            "Selected Nodes",
            self.menu_bar,
        )
        self.menu_bar.addAction(self.load_selected_action)

        self._seperator = QtWidgets.QFrame(self)
        self._seperator.setFrameShape(QtWidgets.QFrame.HLine)
        self._layout.addWidget(self._seperator)

        self.table_view = BoundingBoxReportTableView(self)
        self.table_view.setSortingEnabled(True)
        self._layout.addWidget(self.table_view)


# pylint: disable=invalid-name
class NodeHeaderView(QtWidgets.QHeaderView):
    """This header view selects and zooms to node of clicked header section.

    Shows properties of node if double clicked.

    """

    def __init__(self, orientation=QtCore.Qt.Vertical, parent=None):
        """Construct the header view.

        Args:
            orientation (QtCore.Qt.Orientation): Orientation of the header.
            parent (QtWidgets.QWidget, optional): Parent widget.

        """
        super(NodeHeaderView, self).__init__(orientation, parent)
        if "PySide2" in __binding__:
            self.setSectionsClickable(True)
        elif "PySide" in __binding__:
            self.setClickable(True)

        self.shade_dag_nodes_enabled = nuke_utils.shade_dag_nodes_enabled()

        self.sectionClicked.connect(self.select_node)
        self.sectionDoubleClicked.connect(self.show_properties)

    def paintSection(self, painter, rect, index):
        """Mimic Nuke's way of drawing nodes in DAG.

        Args:
            painter (QtGui.QPainter): Painter to perform the painting.
            rect (QtCore.QRect): Section to paint in.
            index (QtCore.QModelIndex): Current logical index.

        """

        def _scalar(tpl, multiplier):
            return tuple([multiplier * t for t in tpl])

        painter.save()
        QtWidgets.QHeaderView.paintSection(self, painter, rect, index)
        painter.restore()

        bg_brush = self.model().headerData(
            index, QtCore.Qt.Vertical, QtCore.Qt.BackgroundRole
        )  # type: QtGui.QBrush

        if not bg_brush:
            return super(NodeHeaderView, self).paintSection(painter, rect, index)

        fg_pen = self.model().headerData(
            index, QtCore.Qt.Vertical, QtCore.Qt.ForegroundRole
        )  # type: QtGui.QPen

        if self.shade_dag_nodes_enabled:
            gradient = QtGui.QLinearGradient(rect.topLeft(), rect.bottomLeft())
            gradient.setColorAt(0, bg_brush.color())
            gradient_end_color = _scalar(bg_brush.color().getRgbF()[:3], 0.6)
            gradient.setColorAt(1, QtGui.QColor.fromRgbF(*gradient_end_color))
            painter.fillRect(rect, gradient)
        else:
            painter.fillRect(rect, bg_brush)

        rect_adj = rect
        rect_adj.adjust(-1, -1, -1, -1)
        painter.setPen(fg_pen)
        text = self.model().headerData(index, QtCore.Qt.Vertical, QtCore.Qt.DisplayRole)
        painter.drawText(rect, QtCore.Qt.AlignCenter, text)
        painter.setPen(QtGui.QPen(QtGui.QColor.fromRgbF(0.0, 0.0, 0.0)))
        painter.drawRect(rect_adj)

    def get_node(self, section):
        """Return node at current section (index).

        Args:
            section (int): Index of current section in the models header data.

        Returns:
            node (nuke.Node): Node at the current section.

        """
        return self.model().headerData(section, QtCore.Qt.Vertical, QtCore.Qt.UserRole)

    def select_node(self, section):
        """Select node and zoom node graph.

        Args:
            section (int): Index of the node to zoom to.

        """
        node = self.get_node(section)
        nuke_utils.select_node(node, zoom=1)

    def show_properties(self, section):
        """Open properties bin for node at current section.

        Args:
            section (int): Index of the node to show in properties bin.

        """
        node = self.get_node(section)
        nuke.show(node)
