"""Menu integration."""
import nuke
from nukescripts import panels


def get_bounding_box_report_widget():
    import bounding_box_report

    return bounding_box_report.get_gui()


def _add_to_menu():
    menu = nuke.menu('Nuke').findItem("Cache")
    menu.addCommand(
        "Bounding Box Report",
        "import bounding_box_report; bounding_box_report.show_gui()",
    )

    panels.registerWidgetAsPanel(
        "get_bounding_box_report_widget",
        "Bounding Box Report",
        "de.filmkorn.bounding_box_report",
        False,
    )


_add_to_menu()

