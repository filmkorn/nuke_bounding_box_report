nuke_bounding_box_report
------------------------

Simple widget to find nodes with large bounding boxes.

![screenshot](screenshot.png)

Development status: alpha

Example:

```python
import bounding_box_report
bounding_box_report.show_gui()
```

Dock-able widget:

```python
from nukescripts import panels

def get_bounding_box_report_widget():
    import bounding_box_report
    return bounding_box_report.get_gui()


panels.registerWidgetAsPanel(
    'get_bounding_box_report_widget',
    'Bounding Box Report',
    'de.filmkorn.bounding_box_report',
    False
)
```
